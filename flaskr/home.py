import requests

from flask import (
    Blueprint, render_template, jsonify
)

from .scraping import GithubScraping

bp = Blueprint('home', __name__)


def error_handler_api(error):
    code = error.response.status_code
    message = error.response.text
    return jsonify({'error': code, 'message': message})


@bp.route('/')
def main():
    return render_template('home/index.html')


@bp.route('/api/v1.0/forks/<string:owner>/<string:repository>', methods=['GET'])
def get_forks(owner, repository):
    scraper = GithubScraping()
    try:
        repos = scraper.to_scrape(owner, repository, with_last_commit=True)
        response = jsonify({'result': repos})
    except requests.exceptions.HTTPError as error:
        response = error_handler_api(error)

    return response


@bp.route('/api/v1.0/last_commit/<string:owner>/<string:repository>')
def get_last_commit(owner, repository):
    scraper = GithubScraping()
    try:
        repo_name = '{0}/{1}'.format(owner, repository)
        last_commit = scraper.last_commit(repo_name)
        response = jsonify({'result': last_commit})
    except requests.exceptions.HTTPError as error:
        response = error_handler_api(error)

    return response
