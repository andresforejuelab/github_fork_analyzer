import requests
import lxml.html
import json
import logging

log = logging.getLogger(__name__)


def clean_text(text):
    return text.replace('\n', '').replace(' ', '')


def clean_text_basic(text):
    return text.replace('\n', '').strip()


class GithubScraping:

    URL_BASE = 'https://github.com'
    URL_API = 'https://api.github.com'

    def last_commit(self, repo_name):
        """
        Extract the date of the last commit of a repository.
        :param repo_name: string with the following structure <owner>/<repository>
        :return: data of the last commit.
        """
        commits_html = requests.get(self.URL_BASE + '/' + repo_name + '/commits')

        if commits_html.status_code != 200:
            commits_html.raise_for_status()

        commits_doc = lxml.html.fromstring(commits_html.content)
        commits_listing = commits_doc.xpath('//div[contains(@class,"commits-listing")]')[0]
        commit_group_title = commits_listing.xpath('//div[@class="commit-group-title"]')[0]
        text = commit_group_title.text_content()
        text = 'Last {}'.format(text)
        return clean_text_basic(text)

    def last_commit_from_api(self, repo_name):
        """
        Query the date of the last commit of a repository in the github API.
        :param repo_name: string with the following structure <owner>/<repository>
        :return: data of the last commit.
        """
        response = requests.get(self.URL_API + '/repos/' + repo_name + '/commits')
        response_dict = json.loads(response.text)
        result = None
        try:
            result = response_dict[0]['commit']['author']['date']
        except KeyError as e:
            log.error(e)

        return result

    def to_scrape(self, owner, repository, with_last_commit=False):
        forks = []

        response = requests.get('{0}/{1}/{2}/network/members'.format(self.URL_BASE, owner, repository))
        if response.status_code != 200:
            response.raise_for_status()

        doc = lxml.html.fromstring(response.content)
        network = doc.xpath('//div[@id="network"]')[0]
        repos = network.xpath('.//div[@class="repo"]')

        for repo in repos:
            span = repo.xpath('.//span[@class="current-repository"]')
            if span:
                repo_name = clean_text(span[0].text_content())
                repo_url = self.URL_BASE + span[0][-1].get('href')
            else:
                repo_name = clean_text(repo.text_content())
                repo_url = self.URL_BASE + repo[-1].get('href')

            network_tree = repo.xpath('.//img[@class="network-tree"]')
            last_commit = None
            if with_last_commit:
                last_commit = self.last_commit(repo_name)

            forks.append((repo_name, repo_url, len(network_tree), last_commit))

        return forks
