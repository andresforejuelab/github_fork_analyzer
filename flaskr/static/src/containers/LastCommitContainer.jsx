import React, {Component} from 'react';
import axios from 'axios';
import SpinnerIconComponent from '../components/SpinnerIconComponent.jsx';
import TagComponent from '../components/TagComponent.jsx';

class LastCommitContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {last_commit: ''};
  }

  componentDidMount() {
    let repo_uri = this.props.repo_uri;
    axios.get('/api/v1.0/last_commit/' + repo_uri)
      .then(response => {
        console.log(response);
        this.setState({
          last_commit: response.data.result
        });
      })
      .catch(error => {
        // handle error
        console.log(error);
      });
  }

  render() {
    return (
      <div className="control">
        {this.state.last_commit ? (
          <TagComponent text={this.state.last_commit}/>
        ) : (
          <SpinnerIconComponent size="is-normal"/>
        )}
      </div>
    );
  }
}

export default LastCommitContainer;