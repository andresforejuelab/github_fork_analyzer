import React, {Component} from 'react';
import axios from 'axios';
import ColumnsComponent from '../components/ColumnsComponent.jsx';
import ColumnComponent from '../components/ColumnComponent.jsx';
import SearchComponent from '../components/SearchComponent.jsx';
import ResponseDisplayComponent from '../components/ResponseDisplayComponent.jsx';
import ResponseLoadingComponent from '../components/ResponseLoadingComponent.jsx';

class MainContainer extends Component {
  state = {
    searchLoading: false,
    searchResponse: []
  }

  handleSubmit = event => {
    event.preventDefault();
    //console.log(this.form.owner.value);

    this.setState({searchLoading: true});

    const owner = this.form.owner.value;
    const repository = this.form.repository.value;
    axios.get('/api/v1.0/forks/' + owner + '/' + repository)
      .then(response => {
        console.log(response);
        this.setState({
          searchLoading: false,
          searchResponse: response.data.result
        });
      })
      .catch( error => {
        // handle error
        console.log(error);
      });
  }

  setFormRef = element => {
    this.form = element;
  }

  render() {
    return (
      <ColumnsComponent>
        <ColumnComponent>
          <SearchComponent
            setRef={this.setFormRef}
            handleSubmit={this.handleSubmit}/>
        </ColumnComponent>
        <ColumnComponent>
          {this.state.searchLoading ? (
            <ResponseLoadingComponent />
          ) : (
            <ResponseDisplayComponent data={this.state.searchResponse}/>
          )}
        </ColumnComponent>
      </ColumnsComponent>
    );
  }
}

export default MainContainer;