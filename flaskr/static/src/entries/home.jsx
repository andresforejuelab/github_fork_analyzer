import React from "react";
import ReactDOM from "react-dom";
import MainContainer from '../containers/MainContainer.jsx';

const Index = () => {
  return <MainContainer/>;
};

ReactDOM.render(<Index />, document.getElementById("index"));