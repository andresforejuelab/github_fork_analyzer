import React from 'react';

function RepositoryLinkComponent(props) {
  return (
    <div className="control">
      <a href={props.link}>{props.text}</a>
    </div>
  );
}

export default RepositoryLinkComponent;