import React from 'react';
import PanelComponent from './PanelComponent.jsx';
import ForksListComponent from './ForksListComponent.jsx';

function ResponseDisplayComponent(props) {
  return (
    <PanelComponent additionalClasses="has-text-centered">
      <ForksListComponent data={props.data}/>
    </PanelComponent>
  );
}

export default ResponseDisplayComponent;