import React from 'react';
import PanelComponent from './PanelComponent.jsx'

function SearchComponent(props) {
  return (
    <PanelComponent>
      <form
        ref={props.setRef}
        onSubmit={props.handleSubmit} >
        <div className="field">
          <label className="label">Owner</label>
          <div className="control">
            <input className="input" type="text" name="owner"/>
          </div>
        </div>
        <div className="field">
          <label className="label">Repository</label>
          <div className="control">
            <input className="input" type="text" name="repository"/>
          </div>
        </div>
        <div className="control">
          <input className="button is-dark" type="submit" value="Search"/>
        </div>
      </form>
    </PanelComponent>
  );
}

export default SearchComponent;