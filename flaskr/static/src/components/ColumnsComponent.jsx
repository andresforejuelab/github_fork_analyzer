import React from 'react';

function ColumnsComponent(props) {
  return (
    <div className="columns">
      {props.children}
    </div>
  );
}

export default ColumnsComponent;