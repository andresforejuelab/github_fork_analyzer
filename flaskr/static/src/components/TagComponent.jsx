import React from 'react';

function TagComponent(props) {
  return (
    <span className="tag is-link is-normal">{props.text}</span>
  );
}

export default TagComponent;