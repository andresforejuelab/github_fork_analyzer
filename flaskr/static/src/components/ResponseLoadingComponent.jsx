import React from 'react';
import SpinnerIconComponent from "./SpinnerIconComponent.jsx";
import PanelComponent from './PanelComponent.jsx'

function ResponseLoadingComponent(props) {
  return (
    <PanelComponent additionalClasses="has-text-centered">
      <SpinnerIconComponent size="is-normal"/>
    </PanelComponent>
  );
}

export default ResponseLoadingComponent;