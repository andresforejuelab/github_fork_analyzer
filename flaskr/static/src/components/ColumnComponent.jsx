import React from "react";

function ColumnComponent(props) {
  return (
    <div className="column">
      {props.children}
    </div>
  );
}

export default ColumnComponent;