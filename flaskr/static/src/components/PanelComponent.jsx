import React from 'react';

function PanelComponent(props) {
  let additionalClasses = '';
  if (props.additionalClasses) {
    additionalClasses = props.additionalClasses;
  }
  return (
    <div className={`box ${additionalClasses}`}>
      {props.children}
    </div>
  );
}

export default PanelComponent;