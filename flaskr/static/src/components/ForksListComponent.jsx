import React from 'react';
import key from 'weak-key';
import LevelComponent from './LevelComponent.jsx';
import LastCommitContainer from '../containers/LastCommitContainer.jsx';
import RepositoryLinkComponent from './RepositoryLinkComponent.jsx';

function ForksListComponent(props) {
  const data = props.data;
  const listItems = data.map(item =>
    <li key={key(item)}>
      <div className="field is-grouped is-grouped-multiline">
        <LevelComponent level={item[2]}/>
        <RepositoryLinkComponent link={item[1]} text={item[0]}/>
        <LastCommitContainer repo_uri={item[0]}/>
      </div>
    </li>
  );
  return (
    <ul>
      {listItems}
    </ul>
  );
}

export default ForksListComponent;