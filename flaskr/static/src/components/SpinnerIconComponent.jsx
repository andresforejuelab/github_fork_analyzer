import React from 'react';

function SpinnerIconComponent(props) {
  return (
    <span className={`tag is-link ${props.size}`}>
      <i className="fas fa-spinner fa-pulse"></i>
    </span>
  );
}

export default SpinnerIconComponent;