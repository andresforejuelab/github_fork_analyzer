import React from 'react';

function LevelComponent(props) {
  const level = props.level;
  const levelIcons = [];
  for (let i = 0; i < level + 1; i++) {
    levelIcons.push(
      <span className="icon has-text-info" key={i}>
        <i className="fas fa-puzzle-piece"></i>
      </span>
    );
  }
  return (
    <div className="control">{levelIcons}</div>
  );
}

export default LevelComponent;