const path = require('path');

module.exports = {
  entry: {
    "home": path.resolve(__dirname, 'flaskr/static/src/entries/home.jsx'),
  },
  output: {
    path: path.resolve(__dirname, 'flaskr/static/dist'),
    filename: 'js/[name].js',
  },
  devServer: {
    port: 9000,
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              camelCase: true,
              sourceMap: true
            }
          }
        ]
      }
    ]
  }
};