# Github fork analyzer

Website that performs scraping in the forks pages of a given repository and allows to view the forks with the date of the last commit.

The application is developed with Flask, ReactJs and Bulma CSS.

## versions

### v1.0
Application developed with Flask, ReactJs and Bulma CSS.

## License
[MIT](https://choosealicense.com/licenses/mit/)